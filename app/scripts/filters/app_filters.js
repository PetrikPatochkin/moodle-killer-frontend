'use strict';
/**
 * Created by Petrik Patochkin on 10.01.2016.
 */

angular.module('moodlerApp')
    // Avatar filter
    .filter('image', function (CONFIG) {
        return function (input) {
            if( input && input.match(/^http(s)?:\/\//i) )
                return  input;
            return input ? (CONFIG.apiEndpoint + CONFIG.avatarsPath + input) : 'images/no-avatar.png';
        }
    })
    // Tasks filters
    .filter('taskStatusToPanelClass', function(){
        return function (input) {
            var class_name;
            switch (input){
                case 0: // new
                    class_name = 'warning';
                    break;
                case 1: // pending
                    class_name = 'info';
                    break;
                case 2: // refused
                    class_name = 'danger';
                    break;
                case 3: // ready
                    class_name = 'success';
                    break;
                case 4: // done
                    class_name = 'green';
                    break;
                default:
                    class_name = 'default';
            }
            return class_name;
        }
    });
