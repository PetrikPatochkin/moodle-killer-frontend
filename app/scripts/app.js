'use strict';
/**
 * @ngdoc overview
 * @name moodlerApp
 * @description
 * # moodlerApp
 *
 * Main module of the application.
 */
angular.element(document).ready(
    function(){
        var initInjector = angular.injector(['ng']);
        var $http = initInjector.get('$http');

        var config_data;
        function load_api_config(){
            $http.get('api/config')
                .then(function (response) {
                        localStorage.setItem('config',JSON.stringify(response.data));
                        angular.module('moodlerApp').constant('CONFIG', response.data);
                        angular.bootstrap(document, ['moodlerApp']);
                    }
                );
        }
        if(localStorage.getItem('config')){
            try{
                config_data = JSON.parse(localStorage.getItem('config'));
                angular.module('moodlerApp').constant('CONFIG', config_data);
                angular.bootstrap(document, ['moodlerApp']);
            }catch(err){
                load_api_config();
            }
        } else {
            load_api_config();
        }

    }
);

angular
    .module('moodlerApp', [
        'oc.lazyLoad',
        'ui.router',
        'ui.bootstrap',
        'angular-loading-bar',
        'satellizer'
    ])
    .config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
        cfpLoadingBarProvider.includeSpinner = false;
    }])
    .config(
        ['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$authProvider','CONFIG',
            function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $authProvider, CONFIG) {
                $ocLazyLoadProvider.config({
                    debug: false,
                    events: true
                });

                // Auth provider configuration
                $authProvider.loginUrl = '/api/login';
                $authProvider.facebook({
                    clientId: CONFIG.facebook_client_id,
                    url: '/api/login/facebook',
                    name: 'Facebook',
                    authorizationEndpoint: 'https://www.facebook.com/v2.5/dialog/oauth',
                    redirectUri: window.location.origin + '/',
                    requiredUrlParams: ['display', 'scope'],
                    scope: ['email', 'public_profile'],
                    scopeDelimiter: ',',
                    display: 'popup',
                    type: '2.0',
                    popupOptions: {width: 580, height: 400}
                });
                $authProvider.google({
                    clientId: CONFIG.google_client_id,
                    url: '/api/login/google',
                    authorizationEndpoint: 'https://accounts.google.com/o/oauth2/auth',
                    redirectUri: window.location.origin + '/',
                    requiredUrlParams: ['scope'],
                    optionalUrlParams: ['display'],
                    scope: ['profile', 'email'],
                    scopePrefix: 'openid',
                    scopeDelimiter: ' ',
                    display: 'popup',
                    type: '2.0',
                    popupOptions: {width: 580, height: 400}
                });
                // --------------------------

                $urlRouterProvider.otherwise('/home');

                $stateProvider
                    .state('student', {
                        templateUrl: 'views/student-template.html',
                        resolve: {
                            loadMyDirectives: function ($ocLazyLoad) {
                                return $ocLazyLoad.load(
                                    {
                                        name: 'moodlerApp',
                                        files: [
                                            'scripts/directives/header/header.js',
                                            'scripts/directives/header/header-notification/header-notification.js',
                                            'scripts/directives/sidebar/sidebar.js',
                                            'scripts/directives/sidebar/sidebar-search/sidebar-search.js',
                                            'scripts/filters/time.js',
                                            'scripts/filters/app_filters.js',
                                            'scripts/directives/tasks/jira/table.js',
                                            'scripts/directives/tasks/jira/column/column.js'
                                        ]
                                    }),
                                    $ocLazyLoad.load(
                                        {
                                            name: 'toggle-switch',
                                            files: ["bower_components/angular-toggle-switch/angular-toggle-switch.min.js",
                                                "bower_components/angular-toggle-switch/angular-toggle-switch.css"
                                            ]
                                        }),
                                    $ocLazyLoad.load(
                                        {
                                            name: 'ngAnimate',
                                            files: ['bower_components/angular-animate/angular-animate.js']
                                        })
                                $ocLazyLoad.load(
                                    {
                                        name: 'ngCookies',
                                        files: ['bower_components/angular-cookies/angular-cookies.js']
                                    })
                                $ocLazyLoad.load(
                                    {
                                        name: 'ngResource',
                                        files: ['bower_components/angular-resource/angular-resource.js']
                                    })
                                $ocLazyLoad.load(
                                    {
                                        name: 'ngSanitize',
                                        files: ['bower_components/angular-sanitize/angular-sanitize.js']
                                    })
                                $ocLazyLoad.load(
                                    {
                                        name: 'ngTouch',
                                        files: ['bower_components/angular-touch/angular-touch.js']
                                    })
                            }
                        }
                    })
                    .state('student.home', {
                        url: '/home',
                        controller: 'MainCtrl',
                        templateUrl: 'views/dashboard/home.html',
                        resolve: {
                            loadMyFiles: function ($ocLazyLoad) {
                                return $ocLazyLoad.load({
                                    name: 'moodlerApp',
                                    files: [
                                        'scripts/controllers/main.js',
                                        'scripts/directives/timeline/timeline.js',
                                        'scripts/directives/notifications/notifications.js',
                                        'scripts/directives/chat/chat.js',
                                        'scripts/directives/dashboard/stats/stats.js'
                                    ]
                                })
                            }
                        }
                    })
                    .state('student.tasks', {
                        templateUrl: 'views/tasks/list.html',
                        url: '/tasks?d&r&su&sp&as',
                        params:{
                            d: null,
                            r: null,
                            su: null,
                            sp: null,
                            as: null
                        },
                        controller: 'TasksController',
                        resolve: {
                            loadMyFiles: function($ocLazyLoad){
                                return $ocLazyLoad.load({
                                    name: 'moodlerApp',
                                    files: [
                                        'scripts/controllers/tasks.js',
                                        'scripts/directives/tasks/modal-search/search.js',
                                        'scripts/directives/tasks/status/status.js'
                                    ]
                                });
                            }
                        }
                    })
                    .state('student.tasks-created', {
                        templateUrl: 'views/tasks/list-created.html',
                        url: '/tasks/created',
                        controller: 'TasksController',
                        resolve: {
                            loadMyFiles: function($ocLazyLoad){
                                return $ocLazyLoad.load({
                                    name: 'moodlerApp',
                                    files: [
                                        'scripts/controllers/tasks.js',
                                        'scripts/directives/tasks/status/status.js'
                                    ]
                                });
                            }
                        }
                    })
                    .state('student.task-editor', {
                        templateUrl: 'views/tasks/editor.html',
                        url: '/tasks/editor/:task_id',
                        controller: 'TasksController',
                        resolve: {
                            loadMyFiles: function($ocLazyLoad){
                                return $ocLazyLoad.load({
                                    name: 'moodlerApp',
                                    files: [
                                        'scripts/controllers/tasks.js'
                                    ]
                                });
                            }
                        }
                    })
                    .state('student.task', {
                        templateUrl: 'views/tasks/view.html',
                        url: '/task/:task_id',
                        controller: 'TasksController',
                        resolve: {
                            loadMyFiles: function($ocLazyLoad){
                                return $ocLazyLoad.load({
                                    name: 'moodlerApp',
                                    files: [
                                        'scripts/controllers/tasks.js',
                                        'scripts/directives/tasks/status/status.js'
                                    ]
                                });
                            }
                        }
                    })
                    .state('student.task-flow', {
                        templateUrl: 'views/tasks/view_flow.html',
                        url: '/task/:task_id/:student_id',
                        controller: 'TasksController',
                        resolve: {
                            loadMyFiles: function($ocLazyLoad){
                                return $ocLazyLoad.load({
                                    name: 'moodlerApp',
                                    files: [
                                        'scripts/controllers/tasks.js',
                                        'scripts/directives/tasks/status/status.js'
                                    ]
                                });
                            }
                        }
                    })
                    .state('student.departments', {
                        templateUrl: 'views/departments/list.html',
                        url: '/departments',
                        controller: 'DepartmentsController',
                        resolve: {
                            loadMyFiles: function($ocLazyLoad){
                                return $ocLazyLoad.load({
                                    name: 'moodlerApp',
                                    files: [
                                        'scripts/controllers/departments.js'
                                    ]
                                });
                            }
                        }
                    })
                    .state('student.departments-created', {
                        templateUrl: 'views/departments/list-created.html',
                        url: '/departments/created',
                        controller: 'DepartmentsController',
                        resolve: {
                            loadMyFiles: function($ocLazyLoad){
                                return $ocLazyLoad.load({
                                    name: 'moodlerApp',
                                    files: [
                                        'scripts/controllers/departments.js'
                                    ]
                                });
                            }
                        }
                    })
                    .state('student.department', {
                        templateUrl: 'views/departments/view.html',
                        url: '/department/:department_id',
                        controller: 'DepartmentsController',
                        resolve: {
                            loadMyFiles: function($ocLazyLoad){
                                return $ocLazyLoad.load({
                                    name: 'moodlerApp',
                                    files: [
                                        'scripts/controllers/departments.js',
                                        'scripts/directives/departments/modal-invite/invite.js'
                                    ]
                                });
                            }
                        }
                    })
                    .state('student.department-editor', {
                        templateUrl: 'views/departments/editor.html',
                        url: '/department/editor/:department_id',
                        controller: 'DepartmentsController',
                        resolve: {
                            loadMyFiles: function($ocLazyLoad){
                                return $ocLazyLoad.load({
                                    name: 'moodlerApp',
                                    files: [
                                        'scripts/controllers/departments.js'
                                    ]
                                });
                            }
                        }
                    })
                    .state('student.form', {
                        templateUrl: 'views/form.html',
                        url: '/form'
                    })
                    .state('student.blank', {
                        templateUrl: 'views/pages/blank.html',
                        url: '/blank'
                    })
                    .state('student.profile', {
                        templateUrl: 'views/profile.html',
                        url: '/profile/:user_id',
                        controller: 'ProfileController',
                        resolve: {
                            loadMyFile: function ($ocLazyLoad) {
                                return $ocLazyLoad.load({
                                        name: 'moodlerApp',
                                        files: ['scripts/controllers/profile.js']
                                    });
                            }
                        }
                    })
                    .state('student.settings', {
                        templateUrl: 'views/settings.html',
                        url: '/settings',
                        controller: 'SettingsController',
                        resolve: {
                            loadMyFile: function ($ocLazyLoad) {
                                return $ocLazyLoad.load({
                                    name: 'moodlerApp',
                                    files: ['scripts/controllers/settings.js']
                                }),
                                    $ocLazyLoad.load({
                                        name: 'ngFileUpload',
                                        files: ['bower_components/ng-file-upload/ng-file-upload.min.js']
                                    }),
                                    $ocLazyLoad.load({
                                        name: 'ngImgCrop',
                                        files: ['bower_components/ng-img-crop/compile/minified/ng-img-crop.js',
                                            'bower_components/ng-img-crop/compile/minified/ng-img-crop.css']
                                    });
                            }
                        }
                    })
                    .state('login', {
                        templateUrl: 'views/pages/login.html',
                        url: '/login',
                        controller: 'LoginController',
                        resolve: {
                            loadMyFile: function ($ocLazyLoad) {
                                return $ocLazyLoad.load({
                                    name: 'moodlerApp',
                                    files: ['scripts/controllers/login.js']
                                });
                            }
                        }
                    })
                    .state('student.chart', {
                        templateUrl: 'views/chart.html',
                        url: '/chart',
                        controller: 'ChartCtrl',
                        resolve: {
                            loadMyFile: function ($ocLazyLoad) {
                                return $ocLazyLoad.load({
                                    name: 'chart.js',
                                    files: [
                                        'bower_components/angular-chart.js/dist/angular-chart.min.js',
                                        'bower_components/angular-chart.js/dist/angular-chart.css'
                                    ]
                                }),
                                    $ocLazyLoad.load({
                                        name: 'moodlerApp',
                                        files: ['scripts/controllers/chartContoller.js']
                                    })
                            }
                        }
                    })
                    .state('student.table', {
                        templateUrl: 'views/table.html',
                        url: '/table'
                    })
                    .state('student.panels-wells', {
                        templateUrl: 'views/ui-elements/panels-wells.html',
                        url: '/panels-wells'
                    })
                    .state('student.buttons', {
                        templateUrl: 'views/ui-elements/buttons.html',
                        url: '/buttons'
                    })
                    .state('student.notifications', {
                        templateUrl: 'views/ui-elements/notifications.html',
                        url: '/notifications'
                    })
                    .state('student.typography', {
                        templateUrl: 'views/ui-elements/typography.html',
                        url: '/typography'
                    })
                    .state('student.icons', {
                        templateUrl: 'views/ui-elements/icons.html',
                        url: '/icons'
                    })
                    .state('student.grid', {
                        templateUrl: 'views/ui-elements/grid.html',
                        url: '/grid'
                    })
            }
        ])
    .run(function ($rootScope, $state, $auth) {
        $rootScope.$on('$viewContentLoaded', function (next, current) {
            if (!$state.is('login') && !$auth.isAuthenticated()) {
                $state.go('login');
            }
            if ($state.is('login') && $auth.isAuthenticated()) {
                $state.go('student.home');
            }

            var token = $auth.getToken();
            if(token){
                var token_parts = token.split('.');
                var token_payload = JSON.parse(atob(token_parts[1]));
                $rootScope.auth_user_id = token_payload.sub;
            }
        });
    });
