'use strict';

/**
 * @ngdoc directive
 * @name modalTaskSearch
 * @description
 */
angular.module('moodlerApp')
    .directive('modalInvite',function($http, $state, Storage, Notify) {
        return {
            templateUrl: 'scripts/directives/departments/modal-invite/invite.html',
            restrict: 'E',
            replace: true,
            scope: {
                departmentId: '='
            },
            link: function(scope){
                scope.bootModal = function(){
                    scope.loader_search_users = false;
                    if(!scope.users){
                        scope.loadUsersAndRoles();
                        scope.emails_invites = [];
                    }
                };
                scope.loadUsersAndRoles = function(){
                    scope.loader_search_users = true;
                    $http.get('/api/search/users')
                        .then(function (data) {
                            scope.users = data.data;
                            scope.loader_search_users = false;
                        });
                    $http.get('/api/search/roles?department_id='+scope.departmentId)
                        .then(function (data) {
                            scope.roles = data.data;
                        });
                };
                scope.userSwitch = function(user){
                    user.invited = undefined===user.invited||!user.invited;
                };
                scope.addEmailInvite = function(){
                    var obj = {
                        email:'',
                        role_id:''
                    };
                    scope.emails_invites.push(obj);
                    console.log(scope.emails_invites);
                };
                scope.removeEmailInvite = function(email){
                    scope.emails_invites.splice(scope.emails_invites.indexOf(email), 1);
                };
                scope.sendInvites = function(){
                    var users_ids_with_roles_ids = [];
                    var emails_with_roles_ids = [];
                    scope.errors = [];

                    angular.forEach(scope.users,function(val){
                        if(val.invited){
                            if( isNaN(val.role_id)){
                                scope.errors.push("Please select role for "+val.name)
                            } else {
                                users_ids_with_roles_ids.push({
                                    id: val.id,
                                    role_id: val.role_id
                                });
                            }
                        }
                    });
                    angular.forEach(scope.emails_invites,function(val){
                        if( val.email == '' ) {}
                        //else if(!/\S+@\S+\.\S+/.test(val.email)){
                        else if(!/\S+@\S+/.test(val.email)){
                            scope.errors.push("Please provide correct email for "+val.email)
                        }
                        else if( val.role_id=='' || isNaN(val.role_id)){
                            scope.errors.push("Please select role for "+val.email);
                        } else {
                            emails_with_roles_ids.push({
                                email: val.email,
                                role_id: val.role_id
                            });
                        }
                    });

                    if(scope.errors.length == 0){
                        scope.sending_invites = true;
                        var data = {
                            users_ids_with_roles_ids: users_ids_with_roles_ids,
                            emails_with_roles_ids: emails_with_roles_ids
                        };
                        $http.post('/api/invite/send/'+scope.departmentId, data)
                            .then(function(data){
                                scope.emails_invites = [];
                                angular.forEach(scope.users,function(val){
                                    val.invited = false;
                                    val.role_id = undefined;
                                });

                                Notify.success(data.data.status);
                                $('#inviteModal').modal('hide');
                                scope.sending_invites = false;
                            },function(data){
                                angular.forEach(data.data,function(val){
                                    scope.errors.push(val);
                                });
                                scope.sending_invites = false;
                            })
                    }
                };
            }
        }
    });
