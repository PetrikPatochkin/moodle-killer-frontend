'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('moodlerApp')
	.directive('header',function(){
		return {
            templateUrl:'scripts/directives/header/header.html',
            restrict: 'E',
            replace: true,
            controller:function($scope,$auth,$state,Storage){
                $scope.logout = function(){
                    $auth.logout();
                    Storage.clear();
                    $state.go('login');
                }
            }
    	}
	});


