'use strict';

/**
 * @ngdoc directive
 * @name taskStatus
 * @description
 */
angular.module('moodlerApp')
    .directive('tasksJiraColumn',function() {
        return {
            templateUrl: 'scripts/directives/tasks/jira/column/column.html',
            restrict: 'E',
            replace: true,
            transclude: true,
            scope: {
                allBoxes: '=',
                status: '@'
            },
            link: function(scope){
                scope.$watch(function(){return scope.allBoxes;},function(){
                    scope.myboxes = undefined!==scope.allBoxes ? scope.allBoxes[scope.status] : [];
                });
            }
        }
    });
