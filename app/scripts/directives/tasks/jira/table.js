'use strict';

/**
 * @ngdoc directive
 * @name taskStatus
 * @description
 */
angular.module('moodlerApp')
    .directive('tasksJiraTable',function() {
        return {
            templateUrl: 'scripts/directives/tasks/jira/table.html',
            restrict: 'E',
            replace: true,
            scope: {
                taskWithAssigned: '=',
                tasks: '='
            },
            link: function(scope){
                // List of assigned to task
                if(scope.taskWithAssigned){
                    scope.$watch("taskWithAssigned",function(){
                        scope.all_boxes = [[],[],[],[],[]];
                        angular.forEach(undefined===scope.taskWithAssigned ? [] : scope.taskWithAssigned.student_tasks,
                            function(studentTask){
                                var box = {};
                                box.title = studentTask.assigned.user.name;
                                box.number = "#"+scope.taskWithAssigned.id;
                                box.number_link = "student.task-flow({task_id:"+scope.taskWithAssigned.id+",student_id:"
                                    +studentTask.assigned.id+"})";
                                box.name = scope.taskWithAssigned.name;
                                box.image = studentTask.assigned.user.avatar;
                                box.image_name = studentTask.assigned.user.name;
                                box.image_link = "student.profile({user_id:"+studentTask.assigned.user.id+"})";
                                box.deadline = scope.taskWithAssigned.deadline;

                                scope.all_boxes[studentTask.status].push(box);
                            }
                        );
                    });
                }
                // My tasks list
                if(scope.tasks){
                    scope.all_boxes = [[],[],[],[],[]];
                    scope.$watch(function(){return scope.tasks;},function(){
                        scope.all_boxes = [[],[],[],[],[]];
                        angular.forEach(undefined===scope.tasks ? [] : scope.tasks, function(task){
                            var box = {};
                            box.title = '';
                            box.number = "#"+task.id;
                            box.number_link = "student.task-flow({task_id:"+task.id+",student_id:"
                                +task.user_assigned_id+"})";
                            box.name = task.name;
                            box.image = task.creator.user.avatar;
                            box.image_name = task.creator.user.name;
                            box.image_link = "student.profile({user_id:"+task.creator.id+"})";
                            box.deadline = task.deadline;

                            scope.all_boxes[task.status].push(box);
                        });
                    });
                }
            }
        }
    });
