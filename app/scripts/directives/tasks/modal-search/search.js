'use strict';

/**
 * @ngdoc directive
 * @name modalTaskSearch
 * @description
 */
angular.module('moodlerApp')
    .directive('modalTasksSearch',function($http, $location, $state, $timeout, Storage) {
        return {
            templateUrl: 'scripts/directives/tasks/modal-search/search.html',
            restrict: 'E',
            replace: true,
            scope: {
            },
            link: function(scope, element){
                scope.searchActivated = function(){
                    return Object.keys($location.search()).length>0;
                };
                scope.init = function(){

                };
                scope.bootSearch = function(){
                    if(scope.initialization == undefined){
                        scope.search_1 =  true;
                        scope.search_2 =  false;
                        scope.search_3 =  false;
                        scope.url_params = $location.search();
                        this.loadDepartments();
                    }
                };
                scope.loadDepartments = function(){
                    scope.initialization = true;
                    scope.loader_search_1 = true;
                    $http.get('api/search/departments')
                        .then(function(data){
                            scope.departments = data.data;
                            scope.loader_search_1 = false;
                            if(scope.url_params.d && scope.initialization){
                                scope.department = scope.url_params.d;
                                scope.departmentsChanged(true);
                            } else {
                                scope.department = undefined;
                                scope.initialization = false;
                            }
                        });
                };
                scope.departmentsChanged = function(initialization){
                    if(undefined!==initialization && initialization){
                        scope.initialization = true;
                    }
                    scope.search_2 = true;
                    scope.search_3 = false;
                    scope.loader_search_2 = true;
                    // empty 3rd level data
                    scope.users = [];
                    scope.selected_users = undefined;
                    //
                    scope.requests_box_2 = 3;

                    $http.get('api/search/roles?department_id='+scope.department)
                        .then(function(data){
                            scope.roles = data.data;
                            scope.requests_box_2--;
                            if(scope.url_params.r && scope.initialization){
                                scope.role = scope.url_params.r;
                                scope.rolesChanged(true);
                            } else {
                                scope.role = undefined;
                            }
                        });
                    $http.get('api/search/subjects?department_id='+scope.department)
                        .then(function(data){
                            scope.subjects = data.data;
                            scope.requests_box_2--;
                            if(scope.url_params.su && scope.initialization){
                                scope.subject = scope.url_params.su;
                            } else {
                                scope.subject = undefined;
                            }
                        });
                    $http.get('api/search/sprints?department_id='+scope.department)
                        .then(function(data){
                            scope.sprints = data.data;
                            scope.requests_box_2--;
                            if(scope.url_params.sp && scope.initialization){
                                scope.sprint = scope.url_params.sp;
                            } else {
                                scope.sprint = undefined;
                            }
                        });
                    scope.$watch('requests_box_2',function(){
                        if(scope.requests_box_2 == 0) {
                            scope.loader_search_2 = false;
                            scope.initialization = false;
                        }
                    })
                };
                scope.rolesChanged = function(initialization){
                    scope.search_3 = true;
                    scope.loader_search_3 = true;

                    $http.get('api/search/assigned?department_id='+scope.department+'&role_id='+scope.role)
                        .then(function(data){
                            scope.users = data.data;
                            scope.loader_search_3 = false;
                            scope.initialization = false;
                            if(scope.url_params.as){
                                scope.selected_users = [];
                                angular.forEach(scope.url_params.as,function(val){
                                    scope.selected_users.push(parseInt(val));
                                });

                                angular.forEach(scope.users, function(val){
                                    if(scope.selected_users.indexOf(val.id) > -1){
                                        val.selected = true;
                                    }
                                });
                            } else {
                                scope.selected_users = [];
                            }
                        });
                };
                scope.userSwitch = function(user){
                    user.selected = undefined===user.selected||!user.selected ;
                    scope.selected_users = [];
                    angular.forEach(scope.users,function(val){
                        if(val.selected) scope.selected_users.push(val.id);
                    });
                };
                scope.setSearch = function(){
                    var data = {};
                    data.d = scope.department;
                    data.r = scope.role;
                    data.su = scope.subject;
                    data.sp = scope.sprint;
                    data.as = scope.selected_users;

                    Storage.set('search-tasks',data);
                    $state.go('.',data, {notify: false});
                    // Javascript sucks. without timeout it will handle before URL will be changed. stupid language!
                    $timeout(function(){
                        scope.$parent.loadTasks();
                    });

                };
                scope.clear = function(){
                    scope.department = undefined;
                    scope.role = undefined;
                    scope.subject = undefined;
                    scope.sprint = undefined;
                    scope.initialization = undefined;
                    scope.users = undefined;

                    var data =  {d: undefined,su: undefined,sp:undefined,r:undefined,as:undefined};
                    Storage.set('search-tasks',data);
                    $state.go('.',  data, {notify: false});
                    $timeout(function() {
                        scope.$parent.loadTasks();
                    });
                }
            }
        }
    });
