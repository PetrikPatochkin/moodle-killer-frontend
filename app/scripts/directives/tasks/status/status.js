'use strict';

/**
 * @ngdoc directive
 * @name taskStatus
 * @description
 */
angular.module('moodlerApp')
    .directive('taskStatus',function() {
        return {
            template:'<span class="label label-{{ class_name }}" ng-show="show">{{ status_text }}</span>',
            restrict:'E',
            replace:true,
            scope: {
                status: '='
            },
            link: function(scope){
                scope.$watch("status",function() {
                    scope.show = true;
                    switch (scope.status){
                        case null:
                            scope.show = false;
                            break;
                        case 0: // new
                            scope.status_text = 'New';
                            scope.class_name = 'warning';
                            break;
                        case 1: // pending
                            scope.status_text = 'Pending';
                            scope.class_name = 'info';
                            break;
                        case 2: // refused
                            scope.status_text = 'Refused';
                            scope.class_name = 'danger';
                            break;
                        case 3: // ready
                            scope.status_text = 'Ready';
                            scope.class_name = 'success';
                            break;
                        case 4: // done
                            scope.status_text = 'Done';
                            scope.class_name = 'primary';
                            break;
                        default:
                            scope.status_text = 'unrecognized';
                            scope.class_name = 'default';
                    }
                });

            }
        }
    });
