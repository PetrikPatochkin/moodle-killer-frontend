'use strict';
/**
 * Created by Petrik Patochkin on 09.01.2016.
 */
angular.module('moodlerApp')
    .factory('Notify', function () {
        return {
            _options: function(){
                return {
                    // settings
                    element: 'body',
                    message: '',
                    position: null,
                    allow_dismiss: true,
                    showProgressbar: false,
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                    delay: 5000,
                    timer: 1000,
                    url_target: '_blank',
                    mouse_over: null,
                    animate: {
                        enter: 'animated tada fadeInLeft',
                        exit: 'animated fadeOutUp'
                    },
                    onShow: null,
                    onShown: null,
                    onClose: null,
                    onClosed: null,
                    icon_type: 'class',
                    template: '<div data-notify="container" class="notify col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                    '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                    '<span data-notify="icon"></span> ' +
                    '<span data-notify="title">{1}</span> ' +
                    '<span data-notify="message">{2}</span>' +
                    '</div>' +
                    '<a href="{3}" target="{4}" data-notify="url"></a>' +
                    '</div>'
                }
            },

            success: function(text){
                $.notify({
                    title: '<strong>'+text+'</strong>',
                    message: ''
                }, $.extend({}, {
                    type: 'success'
                }, this._options()));
            },
            error: function(text){
                $.notify({
                    title: '<strong>'+text+'</strong>',
                    message: ''
                },{
                    type: 'danger'
                });
            }
        }
    });