'use strict';
/**
 * Created by Petrik Patochkin on 02.01.2016.
 */

angular.module('moodlerApp')
    .factory('Storage', ['$q','$rootScope','$window','Loader', function($q,$rootScope,$window,Loader) {
        return {
            _storage_support: function () {
                var is_support;
                try {
                    is_support = 'localStorage' in window && window['localStorage'] !== null;
                } catch (e) {
                    is_support = false;
                }
                if (!is_support) alert('Your browser doesn\'t support LocalStorage. Choose another');
                return is_support;
            },
            has: function (elem) {
                if (this._storage_support()) {
                    // check value integrity
                    try {
                        JSON.parse((localStorage.getItem(elem)))
                    } catch (err) {
                        this.remove(elem);
                        return false;
                    }
                    // ---------------------
                    return undefined != localStorage.getItem(elem);
                }
            },
            get: function (elem) {
                if (this._storage_support()) {
                    return this.has(elem) ? JSON.parse((localStorage.getItem(elem))) : undefined;
                }
            },
            set: function (elem, value) {
                if (this._storage_support()) {
                    return localStorage.setItem(elem, (JSON.stringify(value)));
                }
            },
            memory: function (elem, valueOrCallback) {
                if (this._storage_support()) {
                    var factory = this;
                    var deferred = $q.defer();
                    if (factory.has(elem)) {
                        deferred.resolve(factory.get(elem));
                    } else {
                        if (typeof valueOrCallback === 'function') {
                            try {
                                valueOrCallback().then(function (data) {
                                    factory.set(elem, data);
                                    deferred.resolve(data);
                                })
                            } catch (err) {
                                value = valueOrCallback();
                                factory.set(elem, value);
                                deferred.resolve(value);
                            }
                        } else {
                            factory.set(elem, valueOrCallback);
                            deferred.resolve(valueOrCallback);
                        }
                    }
                    return deferred.promise;
                }
            },
            getAndUpdate: function (env, variable, cache_name, valueOrCallback, defaultValue, showLoader) {
                if (this._storage_support()) {
                    var factory = this;
                    if (factory.has(cache_name)) {
                        env[variable] = factory.get(cache_name);
                    }
                    if (typeof valueOrCallback === 'function') {
                        try {
                            showLoader = undefined!==showLoader ? showLoader : true;
                            valueOrCallback().then(function (data) {
                                factory.set(cache_name, data);
                                if(showLoader) Loader.finish();
                                env[variable] = factory.get(cache_name);
                            });
                            if(showLoader && !env[variable]) Loader.start();
                        } catch (err) {
                            factory.set(cache_name, valueOrCallback());
                            env[variable] = factory.get(cache_name);
                        }
                    } else {
                        factory.set(cache_name, valueOrCallback);
                        env[variable] = factory.get(cache_name);
                    }

                    if(defaultValue){
                        env[variable] = undefined===env[variable] ? defaultValue : env[variable];
                    }

                    this.setListener(env, variable, cache_name);
                }
            },
            remove: function (elem) {
                if (this._storage_support()) {
                    return localStorage.removeItem(elem);
                }
            },
            clear: function() {
                if (this._storage_support()) {
                    return localStorage.clear();
                }
            },
            setListener: function(env, variable, cache_name){
                var factory = this;
                angular.element($window).on('storage', function(event) {
                    if (event.originalEvent.key === cache_name) {
                        env[variable] = factory.get(cache_name);
                        $rootScope.$apply();
                    }
                });
            }
        }
    }]);