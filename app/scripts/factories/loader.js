'use strict';

/**
 * Created by Petrik Patochkin on 27.01.2016.
 */

angular.module('moodlerApp')
    .factory('Loader', ['$rootScope',function($rootScope) {
        return {
            start: function(){
                $rootScope.loader = true;
            },
            finish: function(){
                $rootScope.loader = false;
            }
        }
    }]);
