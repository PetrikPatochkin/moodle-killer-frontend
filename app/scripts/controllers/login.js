'use strict';
/**
 * @ngdoc function
 * @name moodler.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the moodlerApp
 */
angular.module('moodlerApp')
    .controller('LoginController', function($scope,$state,$auth,Storage) {
        $scope.login = function(params){
            Storage.clear();
            $auth.login(params)
                .then(function(){
                    $state.go('student.home');
                });
        };

        $scope.authenticate = function(provider){
            Storage.clear();
            $auth.authenticate(provider)
                .then(function(){
                    $state.go('student.home');
                });
        }
    });