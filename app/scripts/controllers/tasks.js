'use strict';
/**
 * @ngdoc function
 * @name moodler.controller:TasksController
 * @description
 * # TasksController
 * Controller of the moodlerApp
 */
angular.module('moodlerApp')
    .controller('TasksController', function($scope, $rootScope, $location, $filter, $state, $stateParams, $http, Storage, Notify, $q) {
        /* Tasks */
        $scope.loadTasks = function(){
            var preset_params = {};
            var data = {};
            if(Object.keys($location.search()).length>0){
                // Init search data from URL
                preset_params = $location.search();

            } else if(Storage.has('search-tasks')){
                preset_params = Storage.get('search-tasks');
                $state.go('.',preset_params, {notify: false});
            }
            data = {
                department_id: preset_params.d,
                role_id: preset_params.r,
                subject_id: preset_params.su,
                sprint_id: preset_params.sp,
                'assigned_ids[]': preset_params.as
            };
            // Retrieve user tasks
            Storage.getAndUpdate($scope,'tasks', 'tasks', function(){
                return $q(function(resolve, reject) {
                    $http.get('/api/tasks', {params: data})
                        .then(function (data) {
                            resolve(data.data);
                        })
                        .catch(function () {
                            reject('error');
                        });
                });
            }, []);
        };
        /* /Tasks */
        /* Created tasks */
        $scope.loadCreatedTasks = function(){
            // Retrieve user tasks
            Storage.getAndUpdate($scope,'tasks', 'tasks-created', function(){
                return $q(function(resolve, reject) {
                    $http.get('/api/tasks-created')
                        .then(function (data) {
                            resolve(data.data);
                        })
                        .catch(function () {
                            reject('error');
                        });
                });
            });
        };
        /* /Created tasks */
        /* Task flow view */
        $scope.task_id = parseInt($stateParams.task_id);
        $scope.student_id = parseInt($stateParams.student_id);
        $scope.loadTaskFlow = function(task_id,student_id){
            // Retrieve user task
            Storage.getAndUpdate($scope,'task', 'task-flow-'+task_id, function(){
                return $q(function(resolve, reject) {
                    $http.get('/api/task/'+task_id+'/'+student_id)
                        .then(function (data) {
                            resolve(data.data);
                        })
                        .catch(function () {
                            reject('error');
                        });
                });
            });
        };
        /* /Task flow view */
        /* Task view with assigned */
        $scope.loadTask = function(task_id){
            // Retrieve user task
            Storage.getAndUpdate($scope,'task', 'task-'+task_id, function(){
                return $q(function(resolve, reject) {
                    $http.get('/api/task/'+task_id)
                        .then(function (data) {
                            resolve(data.data);
                        })
                        .catch(function () {
                            reject('error');
                        });
                });
            }, {assigned:[]});
        };
        /* /Task view with assigned */
        /* Messages */
        $scope.checkChangeStatusAbility = function(attempt_status){
            /*
             *  student:
             *  0: 1
             *  1: 0
             *  2: 1
             *
             *  teacher:
             *  0: 3,4
             *  1: 2,3,4
             *  2: 3,4
             *  3: 2,4
             * */
            if(undefined == $scope.task) return false;
            var isStudent = $rootScope.auth_user_id === $scope.task.assigned.user.id;
            var isCreator = $rootScope.auth_user_id === $scope.task.task.creator.user.id;
            if(isStudent && isCreator){
                // Task created to user by the same user
                return true;
            } else {
                var forCurrentStatus;
                if(isStudent){
                    // Student
                    forCurrentStatus = {
                        0: [1],
                        1: [0],
                        2: [1]
                    }[$scope.task.status];
                }
                if(isCreator){
                    // Creator
                    forCurrentStatus = {
                        0: [3,4],
                        1: [2,3,4],
                        2: [3,4],
                        3: [2,4]
                    }[$scope.task.status];
                }
                return undefined!==forCurrentStatus ? (forCurrentStatus.indexOf(attempt_status) >= 0) : false;
            }
        };
        $scope.sendMessage = function(task_id,student_id,message){
            if(undefined !== message && message.status==''){
                delete message.status;
            }
            if(undefined !== message && message.status){
                message.status = parseInt(message.status);
            }
            if(undefined === message || (undefined === message.status && (undefined===message.text || ''===message.text))) {
                Notify.error('You cannot send blank message');
                return;
            }
            $http.post('/api/task/'+task_id+'/'+student_id+'/message',message)
                .then(function(data){
                    $scope.task.flow.push(data.data);
                    if(data.data.status != null){
                        $scope.task.status = data.data.status;
                    }
                    $scope.message.text = '';
                    $scope.message.status = '';
                }, function(data){
                    Notify.error(data.data.status);
                });
        };
        /* /Messages */
        /* Task editor */
        $scope.task_id = parseInt($stateParams.task_id);
        $scope.showDeadlineHelper = function(){
            $('#deadlineHelper').popover('show');
        };
        $scope.loadEditor = function(task_id){
            $scope.isEditing = !isNaN(task_id);
            $scope.task = {};

            if($scope.isEditing){
                // Task is exist
                // Retrieve user task
                Storage.getAndUpdate($scope,'task', 'task-'+task_id, function(){
                    return $q(function(resolve, reject) {
                        $http.get('/api/task/'+task_id)
                            .then(function (data) {
                                resolve(data.data);
                            })
                            .catch(function () {
                                reject('error');
                            });
                    });
                });
                $scope.$watch(function(){return $scope.task;},function(){
                    // Parse deadline from GMT+0
                    if($scope.task.deadline){
                        var dt = new Date(new Date($scope.task.deadline).getTime() - (new Date()).getTimezoneOffset()*60000);
                        $scope.task.deadline_local = $filter('date')(dt, 'yyyy-MM-dd HH:mm');
                    }
                });
            } else {
                $http.get('/api/search/departments')
                    .then(function (data) {
                        $scope.departments = data.data;
                    });
            }
            $scope.$watch(function(){return $scope.task && $scope.task.department_id},function(department_id){
                if (department_id){
                    $scope.loadUsers(department_id);
                    if(!$scope.isEditing){
                        $scope.loadUserAssigned(department_id);
                        $scope.loadSubjects(department_id);
                        $scope.loadSprints(department_id);
                    }
                }
            });
        };
        $scope.loadUserAssigned = function(){
            $scope.creators = undefined;
            $http.get('/api/search/authUserAssigned?department_id='+$scope.task.department_id)
                .then(function(data){
                    $scope.creators = data.data;
                });
        };
        $scope.loadSubjects = function(){
            $scope.subjects = undefined;
            $http.get('/api/search/subjects?department_id='+$scope.task.department_id)
                .then(function(data){
                    $scope.subjects = data.data;
                    $scope.isChangingSubject = true;
                });
        };
        $scope.loadSprints = function(){
            $scope.sprints = undefined;
            $http.get('/api/search/sprints?department_id='+$scope.task.department_id)
                .then(function(data){
                    $scope.sprints = data.data;
                    $scope.isChangingSprint = true;
                });
        };
        $scope.userSwitch = function(user){
            user.selected = undefined===user.selected||!user.selected ;
            $scope.selected_users = [];
            angular.forEach($scope.users,function(user){
                if(user.selected) $scope.selected_users.push(user.id);
            });
        };
        $scope.userDelete = function(user){
            user.deleted = undefined===user.deleted || !user.deleted ;
            $scope.deleted_users = [];
            angular.forEach($scope.task.student_tasks,function(task){
                if(task.assigned.deleted) $scope.deleted_users.push(task.assigned.id);
            });
        };
        $scope.loadUsers = function(department_id){
            $scope.loader_users = true;
            $http.get('/api/search/assigned?department_id='+department_id)
                .then(function(data){
                    $scope.users = data.data;
                    if(undefined !== $scope.task){
                        angular.forEach($scope.task.student_tasks,function(student_task){
                            angular.forEach($scope.users,function(user){
                                if (user.id === student_task.assigned.id){
                                    $scope.users.splice($scope.users.indexOf(user), 1);
                                }
                            });
                        });
                    }
                    $scope.loader_users = false;
                });
        };
        $scope.save = function(task){
            if($scope.selected_users){
                task.selected_assigned = $scope.selected_users;
            }
            if($scope.deleted_users){
                task.deleted_assigned = $scope.deleted_users;
            }
            // Save deadline in GMT+0
            if(task.deadline_local){
                var dt = new Date(new Date(task.deadline_local).getTime() + (new Date()).getTimezoneOffset()*60000);
                $scope.task.deadline = $filter('date')(dt, 'yyyy-MM-dd HH:mm');
            } else {
                $scope.task.deadline = null;
            }
            $http.post('/api/task', task)
                .then(function(data){
                    Notify.success(data.data.status);
                    if(task.id){
                        Storage.remove('task-'+task.id);
                    }
                    $state.go('student.task-editor',{task_id:data.data.id},{reload:true});
                }, function(data){
                    angular.forEach(data.data,function(val){
                        Notify.error(val);
                    });
                });
        };
        /* /Task editor */
    });