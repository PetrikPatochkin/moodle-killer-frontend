'use strict';
/**
 * @ngdoc function
 * @name moodler.controller:DepartmentsController
 * @description
 * # DepartmentsController
 * Controller of the moodlerApp
 */

angular.module('moodlerApp')
    .controller('DepartmentsController', function($scope, $stateParams, $http, $state, $q, Storage, Notify){
        $scope.loadDepartments = function(){
            Storage.getAndUpdate($scope,'departments', 'departments', function(){
                return $q(function(resolve, reject) {
                    $http.get('/api/departments')
                        .then(function (data) {
                            resolve(data.data);
                        })
                        .catch(function () {
                            reject('error');
                        });
                });
            });
        };
        $scope.loadCreatedDepartments = function(){
            Storage.getAndUpdate($scope,'departments', 'departments-created', function(){
                return $q(function(resolve, reject) {
                    $http.get('/api/departments-created')
                        .then(function (data) {
                            resolve(data.data);
                        })
                        .catch(function () {
                            reject('error');
                        });
                });
            });
        };

        $scope.department_id = parseInt($stateParams.department_id);
        $scope.loadDepartment = function(department_id){
            Storage.getAndUpdate($scope,'department', 'department-'+department_id, function(){
                return $q(function(resolve, reject) {
                    $http.get('/api/department/'+department_id)
                        .then(function (data) {
                            resolve(data.data);
                        })
                        .catch(function () {
                            reject('error');
                        });
                });
            });
        };

        $scope.loadEditorDepartment = function(department_id){
            $scope.isEditing = !isNaN(department_id);

            if($scope.isEditing){
                // Department is exist
                Storage.getAndUpdate($scope,'department', 'department-'+department_id, function(){
                    return $q(function(resolve, reject) {
                        $http.get('/api/department/'+department_id)
                            .then(function (data) {
                                resolve(data.data);
                            })
                            .catch(function () {
                                reject('error');
                            });
                    });
                });
            } else {

            }
        };
        $scope.save = function(department){
            $http.post('/api/department', department)
                .then(function(data){
                    Notify.success(data.data.status);
                    if(department.id){
                        Storage.remove('department-'+department.id);
                    }
                    $state.go('student.department-editor',{department_id:data.data.id},{reload:true});
                }, function(data){
                    angular.forEach(data.data,function(val){
                        Notify.error(val);
                    });
                });
        };

        $scope.saveRole = function(role){
            $http.post('/api/department/'+$scope.department_id+'/role',role)
                .then(function(data){
                    if(!role.id){
                        $scope.department.roles.push(data.data.model);
                    }
                    Storage.set('department-'+$scope.department.id,$scope.department);
                    console.log($scope.department);
                    Notify.success(data.data.status);
                }, function(data){
                    angular.forEach(data.data,function(val){
                        Notify.error(val);
                    });
                });
        }
    });