'use strict';
/**
 * @ngdoc function
 * @name moodler.controller:ProfileController
 * @description
 * # ProfileController
 * Controller of the moodlerApp
 */
angular.module('moodlerApp')
    .controller('ProfileController', function($scope, $stateParams, $http, $q, Storage) {
        $scope.user_id = parseInt($stateParams.user_id) ;

        $scope.loadInfo = function(){
            if(!$scope.user_id) return false;
            // Retrieve profile info
            Storage.getAndUpdate($scope,'info', 'user-info-'+$scope.user_id, function(){
                return $q(function(resolve, reject) {
                    $http.get('/api/user/'+$scope.user_id)
                        .then(function (data) {
                            resolve(data.data);
                        })
                        .catch(function () {
                            reject('error');
                        });
                });
            });
        };
    });