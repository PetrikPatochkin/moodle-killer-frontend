'use strict';
/**
 * @ngdoc function
 * @name moodler.controller:SettingsController
 * @description
 * # SettingsController
 * Controller of the moodlerApp
 */
angular.module('moodlerApp')
    .controller('SettingsController', function($scope, $http, Storage, Notify, Loader, $q, $auth, Upload, $timeout) {
        function response_update_info(data){
            if(data.status == 'success'){
                Storage.remove('profile-info');
                Notify.success("Successfully updated");
                $scope.loadInfo();
            } else {
                Notify.error(data.text);
            }
        }

        $scope.loadInfo = function(){
            // Retrieve profile info
            Storage.getAndUpdate($scope,'info', 'profile-info', function(){
                return $q(function(resolve, reject) {
                    $http.get('/api/user')
                        .then(function (data) {
                            resolve(data.data);
                        })
                        .catch(function () {
                            reject('error');
                        });
                });
            });
        };

        $scope.changeAvatar = false;
        $scope.toogleAvatarForm = function(){
            $scope.changeAvatar = !$scope.changeAvatar;
            $scope.showPreviewImagesStatus = 0;
            $scope.showPreviewImages = false;
        };
        $scope.showPreviewImages = false;
        $scope.showPreviewImagesStatus = 0;
        $scope.changePreviewImagesStatus = function(msg){
            if(msg=='loading'){
                $scope.showPreviewImagesStatus = 1;
                $scope.showPreviewImages = false;
                $('#loadAvatarButton').button('loading');
            }
            else
            if(msg=='changed' && $scope.showPreviewImagesStatus == 1){
                $scope.showPreviewImagesStatus = 0;
                $scope.showPreviewImages = true;
                $('#loadAvatarButton').button('reset');
            }
        };

        $scope.upload = function (dataUrl) {
            Loader.start();
            Upload.upload({
                url: '/api/user/update/avatar',
                data: {
                    avatar: Upload.dataUrltoBlob(dataUrl)
                }
            }).then(function (response) {
                $timeout(function () {
                    $scope.progress = 0;
                    $scope.showPreviewImagesStatus = 0;
                    $scope.showPreviewImages = false;
                    $scope.toogleAvatarForm();
                    response_update_info(response.data);
                    Loader.finish();
                });
            }, function (response) {
                if (response.status > 0) $scope.errorMsg = response.status
                + ': ' + response.data;
            }, function (evt) {
                $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
            });
        };

        $scope.update = function(data){
            Loader.start();
            $http.post('/api/user/update',data)
               .then(function(response){
                   response_update_info(response.data);
                   Loader.finish();
               })
        };

        $scope.connect = function(provider){
            Loader.start();
            $auth.link(provider)
                .then(function(response){
                    response_update_info(response.data);
                    Loader.finish();
                },function(){
                    Loader.finish();
                });
        };
    });